﻿using DataTransferObjects;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Linq;
using System.Text;
using RabbitMQ.Client.Events;

namespace RabbitWrap
{
    public class Rabbit
    {
        private readonly ConnectionFactory _connection;

        public Rabbit(ConnectionFactory connection)
        {
            _connection = connection;
        }

        public void SendMessage(User user)
        {
            using (var connection = _connection.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: "firstExchange", type: "topic");
                var message = JsonConvert.SerializeObject(user);
                var body = Encoding.UTF8.GetBytes(message);
                channel.BasicPublish(exchange: "firstExchange",
                    routingKey: "",
                    basicProperties: null,
                    body: body);
                Console.WriteLine($" [x] Sent '{message}'");
            }
        }

        public void ReceiveMessages()
        {
            using (var connection = _connection.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: "firstExchange", type: "topic");

                var queueName = "homework";

                channel.QueueBind(queue: queueName,
                    exchange: "firstExchange",
                    routingKey: string.Empty);

                Console.WriteLine(" [*] Waiting for messages. To exit press CTRL+C");

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body.ToArray());
                    var user = JsonConvert.DeserializeObject<User>(message);

                    var routingKey = ea.RoutingKey;
                    Console.WriteLine($" [x] Received '{message}'");
                    Console.WriteLine($" User name - '{user.Name}', age - '{user.Age}', email - '{user.Email}'.");

                    if (string.IsNullOrEmpty(user.Email) == false)
                    {
                        // Вручную подтверждаем получение сообщения.
                        channel.BasicAck(ea.DeliveryTag, false);
                    }
                };

                channel.BasicConsume(queue: queueName,
                    autoAck: false,
                    consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }
    }
}
