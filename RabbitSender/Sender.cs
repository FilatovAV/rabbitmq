﻿using DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitWrap;

namespace RabbitSender
{
    public class Sender
    {
        private readonly ConnectionFactory _connection;
        private readonly Rabbit _rabbit;
        public Sender()
        {
            _connection =
                new ConnectionFactory()
                {
                    UserName = "rgdsklmv", 
                    Password = "2FW9kWQ14ne9VlY7rT5K9PLL-jlf9QZ1", 
                    VirtualHost = "rgdsklmv", 
                    HostName = "kangaroo.rmq.cloudamqp.com"
                };
            _rabbit = new Rabbit(_connection);
        }
        public void Do()
        {
            while (true)
            {
                var faker = new Bogus.Faker();
                var userFullName = faker.Name.FullName();
                var user = new User()
                {
                    Age = faker.Random.Int(10,110), 
                    Email = faker.Internet.Email(userFullName),
                    Name = userFullName
                };

                _rabbit.SendMessage(user);

                Thread.Sleep(1000);
            }
        }
    }
}
