﻿using System;
using System.Linq;
using System.Text;
using RabbitMQ.Client;

namespace RabbitSender
{
    class Program
    {
        static void Main(string[] args)
        {
            var sender = new Sender();
            sender.Do();
        }
    }
}
